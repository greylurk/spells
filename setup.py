"""
Spells
------

Spells is a database of pathfinder spells available as either a command line
application or as a web-based application.
"""
from setuptools import setup
from setuptools.command.install import install
from load_database import load_data

class install_spell_data(install):
    def run(self):
        install.run(self)
        load_data("spell_full.csv")


setup(
    name='spells',
    version='0.1-dev',
    url='http://github.com/greylurk/spells/',
    license='BSD',
    author='Adam Ness',
    author_email='adam.ness@gmail.com',
    description='Look up Pathfinder spells',
    long_description=__doc__,
    cmdclass={ "install": install_spell_data },
    include_package_data=True,
    zip_safe=False,
    packages=[
        'spells',
        'spells.views'
    ],
    platforms='any',
    install_requires=[
        'Flask>=0.10.1',
        'SQLAlchemy>=0.8.2'
    ],
    classifiers=[
        'Development status :: 4 - Beta',
    ],
)
