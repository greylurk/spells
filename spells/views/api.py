from flask import Blueprint, json, jsonify, make_response, request
from spells.models import Spell

mod = Blueprint("api", __name__)


@mod.route('/autocomplete')
def autocomplete():
    term = request.values.get("term", "")
    spells = Spell.query.filter(Spell.name.like("%s%%" % term)).limit(5)
    names = map(lambda x: x[0], list(spells.values('name')))
    response = make_response(json.dumps(names))
    response.headers['Content-Type'] = 'application/json'
    return response


@mod.route('/spell')
def spell(name=""):
    name = request.values.get("name", "UNKNOWN")
    results = Spell.query.filter(Spell.name.like("%s%%" % name))
    spell = None
    error = None
    if results.count() == 0:
        response = make_response(json.dumps({'error': 'Couldn\'t find that spell'}), 404)
        response.headers['Content-Type'] = 'application/json'
        return response
    else:
        found_spell = results.limit(1)[0]
        spell = {
            'name': found_spell.name,
            'school': found_spell.school,
            'level': found_spell.spell_level,
            'casting_time': found_spell.casting_time,
            'components': found_spell.components,
            'range': found_spell.range,
            'target': found_spell.targets,
            'duration': found_spell.duration,
            'saving_throw': found_spell.saving_throw,
            'spell_resistance': found_spell.spell_resistance,
            'description': found_spell.description_formated
        }
    return jsonify(error=error, spell=spell)
