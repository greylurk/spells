$(document).ready(function() {
	var populate_spell = function( data ) {
		var spell = $("#spell-area");
		$("#spell-searchbox").value = data.spell.name;
		console.log(data.spell.name);
		$.each( data.spell, function(field,value) {
			spell.find("#"+field).html(value);
		});
		return spell;
	};

	var find_spell = function ( spellname ) {
		$.when( $.get(
			$SCRIPT_ROOT + "/api/spell", { 'name': spellname } )
		).done( function( data ) {
			if( data.error ) {
				populate_spell.hide("slide");
				console.log(data);
			} else {
				populate_spell( data ).show("slide");
			}
		}).fail( function( arg1, arg2, arg3) {
			console.log( arg1, arg2, arg3);
		});
	};

	$("#spell-searchbutton").on("click",function(event, ui) {
		var spellname = $("#spell-searchbox").value;
		find_spell( spellname );
		event.preventDefault();
	});

	$("#spell-searchbox").autocomplete({
		source: $SCRIPT_ROOT + "/api/autocomplete",
		change: function(event, ui) {
			var spellname = event.target.value;
			find_spell( spellname );
		}
	});
});


