from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from spells import app

engine = create_engine(app.config['DATABASE_URI'],
                       convert_unicode=False,
                       echo=app.debug,
                       **app.config['DATABASE_CONNECT_OPTIONS'])

db_session = scoped_session(sessionmaker(
    autocommit=False,
    autoflush=False,
    bind=engine
))


def init_db():
    # Importing models so that create_all finds them all
    import spells.models
    Model.metadata.create_all(bind=engine)


Model = declarative_base(name='Model')
Model.query = db_session.query_property()
