import os

_basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = False

DATABASE_URI = 'sqlite:///%s' % os.path.join(_basedir, 'spells.db')
DATABASE_CONNECT_OPTIONS = {}
SECRET_KEY = "development key"

del os

