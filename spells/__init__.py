"""
   Spells
   ~~~~~~

   :copyright: (c) 2013 by Adam Ness.
   :license: BSD, see LICENSE for more details
"""

from flask import Flask, render_template

app = Flask(__name__)
app.config.from_object('spells.websiteconfig')


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@app.teardown_request
def remove_db_session(exception):
    db_session.remove()

from spells.views import main
from spells.views import api

app.register_blueprint(main.mod)
app.register_blueprint(api.mod, url_prefix='/api')

from spells.database import db_session
