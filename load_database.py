from csv import reader

from spells import app
from spells.models import Spell
from spells.database import db_session, init_db


def create_spell(row):
    spell = Spell()
    spell.id = row[70].decode('utf-8')
    spell.name = row[0].decode('utf-8')
    spell.school = row[1].decode('utf-8')
    spell.subschool = row[2].decode('utf-8')
    spell.descriptor = row[3].decode('utf-8')
    spell.spell_level = row[4].decode('utf-8')
    spell.casting_time = row[5].decode('utf-8')
    spell.components = row[6].decode('utf-8')
    spell.costly_components = row[7].decode('utf-8')
    spell.range = row[8].decode('utf-8')
    spell.area = row[9].decode('utf-8')
    spell.effect = row[10].decode('utf-8')
    spell.targets = row[11].decode('utf-8')
    spell.duration = row[12].decode('utf-8')
    spell.dismissible = row[13].decode('utf-8')
    spell.shapeable = row[14].decode('utf-8')
    spell.saving_throw = row[15].decode('utf-8')
    spell.spell_resistance = row[16].decode('utf-8')
    spell.description = row[17].decode('utf-8')
    spell.description_formated = row[18].decode('utf-8')
    spell.source = row[19].decode('utf-8')
    spell.full_text = row[20].decode('utf-8')
    spell.verbal = row[21].decode('utf-8')
    spell.somatic = row[22].decode('utf-8')
    spell.material = row[23].decode('utf-8')
    spell.focus = row[24].decode('utf-8')
    spell.divine_focus = row[25].decode('utf-8')
    spell.sor = row[26].decode('utf-8')
    spell.wiz = row[27].decode('utf-8')
    spell.cleric = row[28].decode('utf-8')
    spell.druid = row[29].decode('utf-8')
    spell.ranger = row[30].decode('utf-8')
    spell.bard = row[31].decode('utf-8')
    spell.paladin = row[32].decode('utf-8')
    spell.alchemist = row[33].decode('utf-8')
    spell.summoner = row[34].decode('utf-8')
    spell.witch = row[35].decode('utf-8')
    spell.inquisitor = row[36].decode('utf-8')
    spell.oracle = row[37].decode('utf-8')
    spell.antipaladin = row[38].decode('utf-8')
    spell.magus = row[39].decode('utf-8')
    spell.deity = row[40].decode('utf-8')
    spell.SLA_Level = row[41].decode('utf-8')
    spell.domain = row[42].decode('utf-8')
    spell.short_description = row[43].decode('utf-8')
    spell.acid = row[44].decode('utf-8')
    spell.air = row[45].decode('utf-8')
    spell.chaotic = row[46].decode('utf-8')
    spell.cold = row[47].decode('utf-8')
    spell.curse = row[48].decode('utf-8')
    spell.darkness = row[49].decode('utf-8')
    spell.death = row[50].decode('utf-8')
    spell.disease = row[51].decode('utf-8')
    spell.earth = row[52].decode('utf-8')
    spell.electricity = row[53].decode('utf-8')
    spell.emotion = row[54].decode('utf-8')
    spell.evil = row[55].decode('utf-8')
    spell.fear = row[56].decode('utf-8')
    spell.fire = row[57].decode('utf-8')
    spell.force = row[58].decode('utf-8')
    spell.good = row[59].decode('utf-8')
    spell.language_dependent = row[60].decode('utf-8')
    spell.lawful = row[61].decode('utf-8')
    spell.light = row[62].decode('utf-8')
    spell.mind_affecting = row[63].decode('utf-8')
    spell.pain = row[64].decode('utf-8')
    spell.poison = row[65].decode('utf-8')
    spell.shadow = row[66].decode('utf-8')
    spell.sonic = row[67].decode('utf-8')
    spell.water = row[68].decode('utf-8')
    spell.linktext = row[69].decode('utf-8')
    spell.material_costs = row[71].decode('utf-8')
    spell.bloodline = row[72].decode('utf-8')
    spell.patron = row[73].decode('utf-8')
    db_session.add(spell)
    db_session.commit()


def load_data(filename):
    init_db()
    counter = 0
    print "Importing from %s" % filename
    with app.open_resource(filename, "rb") as csvfile:
        rows = reader(csvfile)
        rows.next()
        for row in rows:
            create_spell(row)
            counter += 1
    return counter
	

if __name__ == '__main__':
    counter = load_data("spell_full.csv")
    print "Imported %d spells" % counter
